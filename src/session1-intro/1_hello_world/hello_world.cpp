///
// Copyright 2015 - Mark Gooderum (markfrommn@gmail.com)
//

//
// Grab iostream - modern style C++ include (no .h)
//
#include <iostream>

// Be typically lazy and use all of std
using namespace std;

//
// And send Hello World to the output with a newline.
// Note that in C++ main() is main(void) not main(...)
//
int main() {
    cout << "Hello World!" << endl;
    return 0;
}
