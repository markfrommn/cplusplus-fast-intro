///
// Copyright 2015 - Mark Gooderum (markfrommn@gmail.com)
//

//
// Grab headers - modern style C++ include (no .h)
//
#include <iostream>

//
// And lets abuse the preprocessor to do some things
// the compiler cannot do without some help.
//

#define DUMP_VAR(x) do { cout << #x "=" << (x) << endl; cout << "sizeof(" #x ")=" << sizeof(x) << endl; } while (0)
#define DUMP_VAR_PLUS1(x) do { auto x ## _plus_1 = (x) + 1; cout << #x "_plus_1=" << x ## _plus_1 << endl; } while (0)
#define DUMP_VAR_PLUS1_CAST(x) do { auto x ## _plus_1 = (x) + 1; cout << #x "_plus_1=" << (char) (x ## _plus_1) << endl << endl; } while (0)
#define DO_VAR(x) do { DUMP_VAR(x); DUMP_VAR_PLUS1(x); cout << endl; } while (0)
#define DO_COMP(x, y)   if ((x) == (y)) { \
                            cout << x << "==" << y << endl; \
                        } else if ((x) < (y)) { \
                            cout << x << "<" << y << endl; \
                        } else { \
                            cout << x << ">" << y << endl; \
                        }

// Be typically lazy and use all of std
using namespace std;

//
// And send Hello World to the output with a newline.
// Note that in C++ main() is main(void) not main(...)
//
int main() {

    char a_char = 65;
    short a_short = 65;
    int a_int = 65;
    long a_long = 65;
    long long a_long_long = 65;

    float a_float = ( 195.0 / 585.0);
    double a_double_a = ( 195.0 / 585.0);
    double a_double_b = ( 195.0 / 65.0);
    long double a_long_double = ( 195.0 / 65.0);

    const char *a_cstr = "65";
    string a_string = string("65");

    cout << "Hello World, here are some variables..." << endl;

    DO_VAR(a_char);
    DUMP_VAR_PLUS1_CAST(a_char);
    DO_VAR(a_short);
    DO_VAR(a_int);
    DO_VAR(a_long);
    DO_VAR(a_long_long);
    DO_VAR(a_float);
    DO_VAR(a_double_a);
    DO_VAR(a_double_b);
    DO_VAR(a_long_double);
    DO_VAR(a_cstr);
    DUMP_VAR(a_string);

    DO_COMP(a_float, a_double_a);
    DO_COMP(a_float, a_double_b);
    DO_COMP(a_float, a_long_double);


    DO_COMP(a_cstr, a_string);

    return 0;
}

