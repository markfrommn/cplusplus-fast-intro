//
// functions.cpp - Show function definitons and declarations
//
// Created by Mark Gooderum on 6/1/15.
//

#include <iostream>

using namespace std;

double mypow(double val, int exp)
{
    cout << __PRETTY_FUNCTION__ << "(" << val << ", " << exp << ")" << endl;
    if (exp == 0) {
        return(1.0);
    }
    for (int idx = 1; idx < exp; idx++) {
        val *= val;
    }
    return(val);
}


