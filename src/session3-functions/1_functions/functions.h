//
// functions.h - Function declarations for C++ Intro on Functions
//
// Created by Mark Gooderum on 6/2/15.
//

#ifndef __CPPQI_FUNCTIONS_FUNCTIONS_H
#define __CPPQI_FUNCTIONS_FUNCTIONS_H

extern double mypow(double val, int exp);

extern double tpow(double val, int exp);



#endif // __CPPQI_FUNCTIONS_FUNCTIONS_H
