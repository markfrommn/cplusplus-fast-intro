//
// Created by Mark Gooderum on 6/9/15.
//

#include "functions.h"

//
// Grab iostream - modern style C++ include (no .h)
//
#include <iostream>

// Be typically lazy and use all of std
using namespace std;

//
// And send Hello World to the output with a newline.
// Note that in C++ main() is main(void) not main(...)
//
int main() {
    int ival = 2;
    double dval = 2.0;

    const int tpow = 10;

    double dvalpow = mypow(dval, 10);
    double ivalpow = mypow(ival, 2);

    cout << "pow(" << dval << ", " << tpow << ")=" << dvalpow << endl;
    cout << "pow(" << ival << ", " << tpow << ")=" << ivalpow << endl;

    dvalpow = mytpow(dval, 10);
    ivalpow = mytpow(dval, 2);

    cout << "pow(" << dval << ", " << tpow << ")=" << dvalpow << endl;
    cout << "pow(" << ival << ", " << tpow << ")=" << ivalpow << endl;

}
