//
// control_statements.cpp - Show basic control statements
//
// Created by Mark Gooderum on 6/1/15.
//

//
// Grab iostream - modern style C++ include (no .h)
//
#include <iostream>
#include <vector>

// Be typically lazy and use all of std
using namespace std;

//
// And send Hello World to the output with a newline.
// Note that in C++ main() is main(void) not main(...)
//
int main() {
    int a = 1;
    int b = 2; /* dummy */
    int c = -1;
    long long big = 32;
    int d = big;
    std::vector<int> v = /* {0, 1, 2, 3, 4, 5} */
    [](){
        vector<int> vv;
        vv.push_back(1);
        vv.push_back(2);
        vv.push_back(3);
        vv.push_back(4);
        vv.push_back(5);
        return vv;
    }();


    if (c = 0) {
        cout << "C is zero" << endl;
    }
    if ((c = 1) == 1) {
        cout << "C is one" << endl;
    }
    if (a == b)
        cout << "A == B" << endl;
    cout << "Really!! (Not?)" << endl;

    if (a == b)
        cout << "A == B" << endl;
    cout << "Always!!" << endl;

    if (a != b)
        cout << "A != B" << endl;
    else if (a < b)
        cout << "A > B" << endl;
    else
        cout << "A == B" << endl;

    if (a == b)
        ;
    cout << "Always II !!" << endl;

    for (int aa = 0; a < 10; a++)
        cout << "aa=" << aa << endl;

    while (a < 10) {
        a++;
        cout << "a=" << a << endl;
    }

    do {
        a--;
        cout << "a=" << a << endl;
    } while (a > 0);

    switch (a) {
        case 0:
            cout << "a is 0" << endl;

        case 1:
            cout << "a is 1" << endl;
            break;

        default:
            cout << "a is " << a << endl;
    }

    for (auto z : v) {
        cout << "z is " << z << endl;
    }
}
