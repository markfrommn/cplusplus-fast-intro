//
// Created by Mark Gooderum on 6/31/15.
//

//
// Grab iostream - modern style C++ include (no .h)
//
#include <iostream>
#include <cmath>

// Be typically lazy and use all of std
using namespace std;

// Some Global variables with initializers.

int a = 2;

int b = 2 << 2;
int c = 2 * 2; // No exponentiation in C++
float d = sqrt(2.0);
int e = ( ( 2 == 1) ? 1 : 2);
int f[3] = { 2, 1, 3};
int g = int(3);


//
// Show some basic operators. and common statement block idioms.
//
int main() {
    int a = 1;      // Hides global a
    int b = 2;      // etc
    int c = -1;
    long long big = 32;
    int d = big;
    int h[3] = { 2, 3, 4};

    // Arithmetic operators

    cout << "a+1=" << a + 1;

    if (0 == c) {
        cout << "C is zero" << endl;
    }
    if ((c = 1) == 1) {
        cout << "C is one" << endl;
    }
    if (a == b)
        cout << "A == B" << endl;
        cout << "Really!! (Not?)" << endl;

    if (a == b)
        cout << "A == B" << endl;
    cout << "Always!!" << endl;

    if (a != b)
        cout << "A != B" << endl;
    else if (a < b)
        cout << "A > B" << endl;
    else
        cout << "A == B" << endl;

    if (a == b)
        ;
    cout << "Always II !!" << endl;
}
