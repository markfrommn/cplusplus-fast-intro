//
// number_setc.cpp - Implementation for number set based on class.
//
//
// Created by mgooderum on 7/17/15.
//

#include "number_setc.h"

using namespace std;
using namespace mark_nums;

long double
number_setc::Mean()
{
    return(meanval);
}

int
number_setc::Mode()
{
    return(modeval);
}

void
number_setc::Add(int val)
{
    vals.insert(val);
    counts.inert(val);
    if (modeval != val) {
        update_mode();
    }
    update_mean_add(val);
    return;
}

void
number_setc::Remove(int val)
{
    vals.remove(val);
    update_mode();
    update_mean_remove(val);
    return;
}

int
number_setc::Count()
{
    return(vals.size());
}

void
number_setc::update_mean_add(int val)
{
    //
    // Incremental update of mean.
    //
    // We are always called after adding the value,
    // so the old mean is based on set size-1.
    //
    long double new_total= meanval * (vals.size() - 1);
    new_total += val;
    meanval = new_total / vals.size();
    return;
}

void
number_setc::update_mean_remove(int val){
    //
    // Incremental update of mean.
    //
    // We are always called after removing the value,
    // so the old mean is based on set size+1.
    //
    long double new_total= meanval * (vals.size() + 1);
    new_total -= val;
    meanval = new_total / vals.size();
    return;
}

void
number_setc::update_mode()
{
    int mc;
    int mv;

    for (auto &It : vals)
    {

    }
}