//
// number_setc.h - Header for number set based on class.
//
//
// Created by mgooderum on 7/17/15.
//

#ifndef CPLUSPLUS_QUICK_INTRO_NUMBER_SETC_H
#define CPLUSPLUS_QUICK_INTRO_NUMBER_SETC_H

#include <bits/unordered_map.h>
#include "set"

using namespace std;

namespace mark_nums {

    class number_setc {

    public:
        long double Mean();

        int Mode();

        void Add(int val);

        void Remove(int val);

        int Count();

    private:
        multiset<int> vals;
        long double meanval;
        int modeval;

        void update_mean_add(int val);
        void update_mean_remove(int val);

        void update_mode();
    };

}

#endif //CPLUSPLUS_QUICK_INTRO_NUMBER_SETC_H
