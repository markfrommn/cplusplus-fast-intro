//
// number_setc.cpp - Test main for number set based on class.
//
// Created by mgooderum on 7/17/15.
//

#include "number_setc.h"

//
// Grab iostream - modern style C++ include (no .h)
//
#include <iostream>

// Be typically lazy and use all of std
using namespace std;

//
// And send Hello World to the output with a newline.
// Note that in C++ main() is main(void) not main(...)
//
int main() {
    cout << "Done!" << endl;
}