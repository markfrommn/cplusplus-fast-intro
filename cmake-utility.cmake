if ( CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    # Using Clang
    set ( CMAKE_COMPILER_IS_CLANG 1 )
elseif ( CMAKE_CXX_COMPILER_ID MATCHES "AppleClang")
    # Using Clang on Apple
    set ( CMAKE_COMPILER_IS_CLANG 1 )
elseif ( CMAKE_CXX_COMPILER_ID MATCHES "GNU")
    # Using gcc/g++
    set ( CMAKE_COMPILER_IS_GNUCC 1 )
    set ( CMAKE_COMPILER_IS_GNUCXX 1 )
elseif ( CMAKE_CXX_COMPILER_ID MATCHES "Intel")
    # using Intel C++
    set ( CMAKE_COMPILER_IS_INTEL 1 )
elseif ( CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
    # using Visual Studio C++
    set ( CMAKE_COMPILER_IS_MSVC 1 )
endif()

# If Clang is set
if (CMAKE_COMPILER_IS_CLANG)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -stdlib=libc++")
endif()

# If GCC set
if (CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
endif()
